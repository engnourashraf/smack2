//
//  ServiceHelper.swift
//  smack
//
//  Created by Nour on 9/26/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class ServiceHelper {
    static let instance = ServiceHelper()
    var Channels = [ChannelModel]()
    var Messages = [MessageModel]()
    var selectedChannel :ChannelModel = ChannelModel()
    
    func registerUser(email :String , password :String , compliation :@escaping CompilationHandler) {
        let header = [
            "Content-Type" : "application/json; charset = utf-8"
        ]
        
        let body :[String:Any] = [
            "email" : email,
            "password" : password
        ]
      
        Alamofire.request(RegisterUrl, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header)
            .responseString { (response) in
                if response.result.error == nil {
                    compliation(true)
                }else{
                    compliation(false)
                    debugPrint(response.result.error as Any)
                }
        }
    }
    
    
    
    func loginUser(email :String , password :String , compliation :@escaping CompilationHandler) {
        let header = [
            "Content-Type" : "application/json; charset = utf-8"
        ]
        
        let body :[String:Any] = [
            "email" : email,
            "password" : password
        ]
    
        Alamofire.request(LoginUrl, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { (response) in
                if response.result.error == nil {
                    guard let data = response.data else {return}
                    do {
                        let json = try JSON.init(data: data)
                        let email = json["user"].stringValue
                        let token = json["token"].stringValue
                        UserDefaultsHelper.instance.UserEmail = email
                        UserDefaultsHelper.instance.Token = token
                        UserDefaultsHelper.instance.LoggedIn = true
                        compliation(true)
                    }catch{
                        return
                    }

                }else{
                    compliation(false)
                    debugPrint(response.result.error as Any)
                }
        }
    }
    
    
    func createUser(email :String , userName :String ,avatarName : String,avatarColor : String, compliation :@escaping CompilationHandler) {
        let header = [
            "Content-Type" : "application/json; charset = utf-8",
            "Authorization" : UserDefaultsHelper.instance.Token
        ]
        
        let body :[String:Any] = [
            "email" : email,
            "avatarColor" : avatarColor,
            "avatarName" : avatarName,
            "name" : userName
        ]
        
        Alamofire.request(AddUserUrl, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { (response) in
                if response.result.error == nil {
                    guard let data = response.data else {return}
                    do {
                        let json = try JSON.init(data: data)
                        let id = json["_id"].stringValue
                        let email = json["email"].stringValue
                        let avatarName = json["avatarName"].stringValue
                        let name = json["name"].stringValue
                        let avatarColor = json["avatarColor"].stringValue
                        UserDataModel.instance.setUserData(name: name, id: id, avaterColor: avatarColor, avaterName: avatarName, email: email)
                        compliation(true)
                    }catch{
                        return
                    }
                    
                }else{
                    compliation(false)
                    debugPrint(response.result.error as Any)
                }
        }
    }
    
    func FindUserByEmail(compliation :@escaping CompilationHandler)  {
        let header = [
            "Content-Type" : "application/json; charset = utf-8",
            "Authorization" : UserDefaultsHelper.instance.Token
        ]
        
        let body :[String:Any]? = nil
        
        Alamofire.request("\(FindUserByIdUrl)\(UserDefaultsHelper.instance.UserEmail)", method: .get, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                do {
                    let json = try JSON.init(data: data)
                    let id = json["_id"].stringValue
                    let email = json["email"].stringValue
                    let avatarName = json["avatarName"].stringValue
                    let name = json["name"].stringValue
                    let avatarColor = json["avatarColor"].stringValue
                    UserDataModel.instance.setUserData(name: name, id: id, avaterColor: avatarColor, avaterName: avatarName, email: email)
                    compliation(true)
                }catch let error{
                    debugPrint(error as Any)
                    return
                }
                
            }else{
                compliation(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    

    func FindAllChannels(compliation :@escaping CompilationHandler)  {
        let header = [
            "Content-Type" : "application/json; charset = utf-8",
            "Authorization" : UserDefaultsHelper.instance.Token
        ]
        
        let body :[String:Any]? = nil
        
        Alamofire.request(FindChannelsUrl, method: .get, parameters: body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { (response) in
                if response.result.error == nil {
                    guard let data = response.data else {return}
                    do {
                        self.Channels = try JSONDecoder.init().decode([ChannelModel].self,from: data)
//                        print(self.Channels)
                    }catch let error{
                        debugPrint(error as Any)
                        return
                    }
                    compliation(true)
                }else{
                    compliation(false)
                    debugPrint(response.result.error as Any)
                }
        }
    }
    
    func FindMessageByChannel(channelId : String,compliation :@escaping CompilationHandler)  {
        let header = [
            "Content-Type" : "application/json; charset = utf-8",
            "Authorization" : UserDefaultsHelper.instance.Token
        ]
        
        let body :[String:Any]? = nil
        
        Alamofire.request("\(FindMessageByChannelUrl)\(channelId)", method: .get, parameters: body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { (response) in
                if response.result.error == nil {
                    self.Messages.removeAll()
                    guard let data = response.data else {return}
                    do {
                        self.Messages = try JSONDecoder.init().decode([MessageModel].self,from: data)
                        print(self.Messages)
                    }catch let error{
                        debugPrint(error as Any)
                        return
                    }
                    compliation(true)
                }else{
                    compliation(false)
                    debugPrint(response.result.error as Any)
                }
        }
    }
}
