//
//  ServiceSocketObject.swift
//  smack
//
//  Created by Nour on 10/1/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit
import SocketIO
var manager:SocketManager = SocketManager(socketURL: URL(string: BaseUrl)!, config: [.log(true), .compress])
var socketIOClient: SocketIOClient = manager.defaultSocket
class ServiceSocketHelper: NSObject {

    static let instance = ServiceSocketHelper()
    
    override init() {
        super.init()
    }
    

  
    func ConnectToSocket() {
        
        socketIOClient.on(clientEvent: .connect) {data, ack in
            print(data)
            print("socket connected")
        }
        
        socketIOClient.on(clientEvent: .error) { (data, eck) in
            print(data)
            print("socket error")
        }
        
        socketIOClient.on(clientEvent: .disconnect) { (data, eck) in
            print(data)
            print("socket disconnect")
        }
        
        socketIOClient.on(clientEvent: SocketClientEvent.reconnect) { (data, eck) in
            print(data)
            print("socket reconnect")
        }
        
        socketIOClient.connect()
    }
    
    func closeConnection()  {
        socketIOClient.disconnect()
    }
    
    func addChannel(channelName : String ,description : String,compliation :@escaping CompilationHandler)  {
        socketIOClient.emit("newChannel", channelName,description)
        compliation(true)
    }
    
    func getChannel(compliation :@escaping CompilationHandler)  {
        socketIOClient.on("channelCreated") { (dataArray, ack) in
            guard let channelName = dataArray[0] as? String else {return}
            guard let channelDescription = dataArray[1] as? String else {return}
            guard let channelId = dataArray[2] as? String else {return}
            let newChannel = ChannelModel.init(name: channelName, description: channelDescription, _id: channelId, __v: 0)
            ServiceHelper.instance.Channels.append(newChannel)
            compliation(true)
        }
    }
    
    func addMessage(messageBody : String ,channelId : String,userId : String,compliation :@escaping CompilationHandler)  {
        let user = UserDataModel.instance
socketIOClient.emit("newMessage",messageBody,userId,channelId,user.name!,user.avaterName!,user.avaterColor!)
        compliation(true)
    }
    
    func getMessage(compliation :@escaping CompilationHandler)  {
        socketIOClient.on("messageCreated") { (dataArray, ack) in
            guard let msgBody = dataArray[0] as? String else {return}
            guard let channelId = dataArray[2] as? String else {return}
            guard let userName = dataArray[3] as? String else {return}
            guard let userAvatar = dataArray[4] as? String else {return}
            guard let userAvatarColor = dataArray[5] as? String else {return}
            guard let id = dataArray[6] as? String else {return}
            guard let timeStamp = dataArray[7] as? String else {return}
            if channelId == ServiceHelper.instance.selectedChannel._id && UserDefaultsHelper.instance.LoggedIn{
                let newMessage = MessageModel.init(userName: userName, messageBody: msgBody, userId: UserDataModel.instance.id, channelId: channelId, userAvatar: userAvatar, userAvatarColor: userAvatarColor, _id: id, timeStamp: timeStamp, __v: 0)
                ServiceHelper.instance.Messages.append(newMessage)
                compliation(true)
            }else{
                compliation(false)
            }
        }
    }
    
    func getTypingUsers(compliation :@escaping (_ typeingUsers: [String:String] ) -> Void )  {
                socketIOClient.on("userTypingUpdate") { (dataArray, ack) in
                    guard let typingUsers = dataArray[0] as? [String:String] else {return}
                    compliation(typingUsers)
        }
    }
    
    func startTyping()  {
        let Channel = ServiceHelper.instance.selectedChannel._id
        let Name = UserDataModel.instance.name
        socketIOClient.emit("startType",Name!,Channel!)
    }
    
    func stopTyping()  {
        let Channel = ServiceHelper.instance.selectedChannel._id
        let Name = UserDataModel.instance.name
        socketIOClient.emit("stopType",Name!,Channel!)
    }
}
