//
//  CirclurImageView.swift
//  smack
//
//  Created by Nour on 10/1/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit
@IBDesignable
class CirclurImageView: UIImageView {

    
    override func awakeFromNib() {
        setUpView()
    }
    
    override func prepareForInterfaceBuilder() {
        setUpView()
    }
    
    func setUpView() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
    

}
