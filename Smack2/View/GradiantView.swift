//
//  GradiantView.swift
//  smack
//
//  Created by Nour on 9/25/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

@IBDesignable
class GradiantView: UIView {

    @IBInspectable var startColor : UIColor = #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1)  {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var endColor : UIColor = #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1)  {
        didSet {
            self.setNeedsLayout()
        }
    }
 
    override func layoutSubviews() {
        CustomView()
    }
    
    override func prepareForInterfaceBuilder() {
        CustomView()
    }
    
    func CustomView() {
        let GradiantLayer = CAGradientLayer.init()
        GradiantLayer.colors = [startColor.cgColor,endColor.cgColor]
        GradiantLayer.startPoint = CGPoint.init(x: 0, y: 0)
        GradiantLayer.endPoint = CGPoint.init(x: 1, y: 1)
        GradiantLayer.frame = self.bounds
        self.layer.insertSublayer(GradiantLayer, at: 0)
    }

}
