//
//  AvaterCell.swift
//  smack
//
//  Created by Nour on 9/30/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class AvaterCell: UICollectionViewCell {
    @IBOutlet weak var imageCell: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpViews()
    }
    
    func setUpViews()  {
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        self.clipsToBounds = true
        self.layer.cornerRadius = 10
    }
    
    func configureCell(index : Int, type : AvaterColorType)  {
        if type == .dark {
            imageCell.image = UIImage.init(named: "dark\(index)")
            self.layer.backgroundColor = UIColor.lightGray.cgColor
        }else{
            imageCell.image = UIImage.init(named: "light\(index)")
            self.layer.backgroundColor = UIColor.gray.cgColor
        }
    }
}
