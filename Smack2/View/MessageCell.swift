//
//  MessageCell.swift
//  smack
//
//  Created by Nour on 10/2/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var messageBodyLB: UILabel!
    @IBOutlet weak var messageStamp: UILabel!
    @IBOutlet weak var profileNameLB: UILabel!
    @IBOutlet weak var profileImage: CirclurImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(message : MessageModel) {
        let body = message.messageBody
        let name = message.userName
        let imageName = message.userAvatar
        let stamp = message.timeStamp
        let imageBg = ScanColor(color: message.userAvatarColor)
        messageBodyLB.text = body
        messageStamp.text = stamp
        profileNameLB.text = name
        messageStamp.text = stamp
        profileImage.image = UIImage.init(named: imageName!)
        profileImage.backgroundColor = imageBg
    }

}
