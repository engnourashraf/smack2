//
//  ChannelCell.swift
//  smack
//
//  Created by Nour on 10/1/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {

    @IBOutlet weak var titleLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.layer.backgroundColor = UIColor.init(white: 1, alpha: 0.2).cgColor
        }else{
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    func configureView(channel : ChannelModel)  {
        let ChannelName = "#\(channel.name!)"
        titleLB.text = ChannelName
        
    }
}
