//
//  MainBtn.swift
//  smack
//
//  Created by Nour on 9/25/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit
@IBDesignable
class MainBtn: UIButton {

    
    @IBInspectable var CornerRadius :CGFloat  = 0.0 {
        didSet {
            CustomVIew()
//            self.layer.cornerRadius = CornerRadius
        }
    }
    override func awakeFromNib() {
        CustomVIew()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        CustomVIew()
    }
    
    func CustomVIew() {
            self.layer.cornerRadius = CornerRadius
    }
}
