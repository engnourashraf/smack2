//
//  ScanColor.swift
//  smack
//
//  Created by Nour on 10/1/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

func ScanColor(color :String) -> UIColor {
    let scanner = Scanner.init(string: color)
    let skipped = CharacterSet.init(charactersIn: "[],")
    let coma = CharacterSet.init(charactersIn: ",")
    scanner.charactersToBeSkipped = skipped
    var r,b,g,a : NSString?
    scanner.scanUpToCharacters(from: coma, into: &r)
    scanner.scanUpToCharacters(from: coma, into: &g)
    scanner.scanUpToCharacters(from: coma, into: &b)
    scanner.scanUpToCharacters(from: coma, into: &a)
    let DefaultColor = UIColor.lightGray
    guard let rUnwrapped = r else {return DefaultColor}
    guard let gUnwrapped = g else {return DefaultColor}
    guard let bUnwrapped = b else {return DefaultColor}
    guard let aUnwrapped = a else {return DefaultColor}
    
    let rFloat = CGFloat.init(rUnwrapped.doubleValue)
    let gFloat = CGFloat.init(gUnwrapped.doubleValue)
    let bFloat = CGFloat.init(bUnwrapped.doubleValue)
    let aFloat = CGFloat.init(aUnwrapped.doubleValue)

    let newColor = UIColor.init(red: rFloat, green: gFloat, blue: bFloat, alpha: aFloat)
    return newColor
}
