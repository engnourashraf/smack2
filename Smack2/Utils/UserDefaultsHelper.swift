//
//  UserDefaultsHelper.swift
//  smack
//
//  Created by Nour on 9/26/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import Foundation

class UserDefaultsHelper {
    static let instance = UserDefaultsHelper()
    let Defaults = UserDefaults.standard
    
    var LoggedIn :Bool {
        get {
            return Defaults.bool(forKey: LoggedInDefaults)
        }
        
        set{
            Defaults.set(newValue, forKey: LoggedInDefaults)
        }
    }
    
    var UserEmail :String {
        get {
            return Defaults.string(forKey: UserEmailDefaults)!
        }
        
        set{
            Defaults.set(newValue, forKey: UserEmailDefaults)
        }
    }
    
    var Token :String {
        get {
            return Defaults.string(forKey: TokenDefaults)!
        }
        
        set{
            Defaults.set("Bearer \(newValue)", forKey: TokenDefaults)
        }
    }
}
