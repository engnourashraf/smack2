//
//  Constents.swift
//  smack
//
//  Created by Nour on 9/25/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

//Compilation Handler
typealias CompilationHandler = (_ Success : Bool) -> ()

//Segues

let LoginSegue = "toLogin"
let CreateAccountSegue = "toCreateAccount"
let BackToChannelsSegue = "unwindToChannels"
let AvaterSegue = "toAvater"

//User Defaults

let LoggedInDefaults = "LoggedInDefaults"
let UserEmailDefaults = "UserEmailDefaults"
let TokenDefaults = "TokenDefaults"

//Urls
let BaseUrl = "https://chatiosapp.herokuapp.com/v1/"
let RegisterUrl = "\(BaseUrl)account/register"
let LoginUrl = "\(BaseUrl)account/login"
let AddUserUrl = "\(BaseUrl)user/add"
let FindUserByIdUrl = "\(BaseUrl)user/byEmail/"
let FindChannelsUrl = "\(BaseUrl)channel"
let FindMessageByChannelUrl = "\(BaseUrl)message/byChannel/"

//Data
let avaterCellReuseId = "avatarCell"
let channelCellReuseId = "channelCell"
let messageCellReuseId = "messageCell"
let placeHolderColor = #colorLiteral(red: 0.3381048872, green: 0.5963036558, blue: 0.7576039138, alpha: 0.5)

//Notification Center
let LoginStatusNotify = NSNotification.Name(rawValue: "LoginStatusNotify")
let ChannelSelectedNotify = NSNotification.Name(rawValue: "ChannelSelectedNotify")


