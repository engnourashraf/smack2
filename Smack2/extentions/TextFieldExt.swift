//
//  TextFieldExt.swift
//  smack
//
//  Created by Nour on 10/1/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit
extension UITextField {
    func setPlaceHolderColor(text:String,color:UIColor) {
        self.attributedPlaceholder = NSAttributedString.init(string: text, attributes: [NSAttributedString.Key.foregroundColor:color])
    }
}
