//
//  ChannelVC.swift
//  smack
//
//  Created by Nour on 9/25/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit
import SocketIO
class ChannelVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ServiceHelper.instance.Channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: channelCellReuseId) as? ChannelCell {
            let channel = ServiceHelper.instance.Channels[indexPath.item]
            cell.configureView(channel: channel)
            return cell
        }else{
            return ChannelCell()
        }
    }
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var addChannelBtn: UIButton!
    
    @IBOutlet weak var channelsTV: UITableView!
    @IBOutlet weak var profileImage: CirclurImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
        NotificationCenter.default.addObserver(self, selector: #selector(LoginStatusChanged), name: LoginStatusNotify, object: nil)
        LoadProfileInfo()
        LoadChannels()
        channelsTV.delegate = self
        channelsTV.dataSource = self
        ServiceSocketHelper.instance.getChannel { (success) in
            if success {
                self.channelsTV.reloadData()
            }
        }
    }
    
    func LoadProfileInfo()  {
        if UserDefaultsHelper.instance.LoggedIn {
            ServiceHelper.instance.FindUserByEmail(compliation: { (success) in
              self.setInfoData()
            })
        }
    }
    
    func setInfoData() {
        loginBtn.setTitle(UserDataModel.instance.name, for: .normal)
        profileImage.image = UIImage.init(named: UserDataModel.instance.avaterName!)
        profileImage.backgroundColor = ScanColor(color: UserDataModel.instance.avaterColor!)
    }
    @objc func LoginStatusChanged() {
        if UserDefaultsHelper.instance.LoggedIn {
            setInfoData()
            //print color Values
            let color = ScanColor(color: UserDataModel.instance.avaterColor!)
            var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
            color.getRed(&r, green: &g, blue: &b, alpha: &a)
            print("red: \(r), green: \(g), blue: \(b), alpha: \(a)")
            LoadChannels()
        }else{
            loginBtn.setTitle("Login", for: .normal)
            profileImage.image = UIImage.init(named: "profileDefault")
            profileImage.backgroundColor = UIColor.clear
            channelsTV.reloadData()
        }
    }

    @IBAction func addChannelBtnClick(_ sender: Any) {
        if UserDefaultsHelper.instance.LoggedIn {
            let addChannel = AddChannelVC.init()
            addChannel.modalPresentationStyle = .custom
            present(addChannel, animated: true, completion: nil)
        }
    }
    @IBAction func loginBtnClick(_ sender: Any) {
        if UserDefaultsHelper.instance.LoggedIn {
            if UserDataModel.instance.name != nil{
            let profile = ProfileVC.init()
            profile.modalPresentationStyle = .custom
            present(profile, animated: true, completion: nil)
            }
        }else{
        performSegue(withIdentifier: LoginSegue, sender: nil)
        }

    }
    
    @IBAction func BackToChannels(segue : UIStoryboardSegue){
        
    }
    
    func LoadChannels()  {
        if UserDefaultsHelper.instance.LoggedIn {
        ServiceHelper.instance.FindAllChannels { (success) in
            if success {
                self.channelsTV.reloadData()
            }
          }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ServiceHelper.instance.selectedChannel = ServiceHelper.instance.Channels[indexPath.item]
        NotificationCenter.default.post(name: ChannelSelectedNotify, object: nil)
        self.revealViewController().revealToggle(true)
    }
    
}
