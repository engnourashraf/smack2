//
//  ProfileVC.swift
//  smack
//
//  Created by Nour on 10/1/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    @IBOutlet weak var bg: UIView!
    @IBOutlet weak var emailLB: UILabel!
    @IBOutlet weak var profileNameLB: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeScreen))
        bg.addGestureRecognizer(tap)
        setUpView()
        // Do any additional setup after loading the view.
    }
    @IBAction func logoutBtnClick(_ sender: Any) {
        UserDataModel.instance.logout()
        NotificationCenter.default.post(name: LoginStatusNotify, object: nil)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func closeBtnClick(_ sender: Any) {
        closeScreen()
    }
    @objc func closeScreen(){
        dismiss(animated: true, completion: nil)
    }
    
    func setUpView()  {
        profileImage.image = UIImage.init(named: UserDataModel.instance.avaterName!)
        profileImage.backgroundColor = ScanColor(color: UserDataModel.instance.avaterColor!)
        emailLB.text = UserDataModel.instance.email
        profileNameLB.text = UserDataModel.instance.name
    }
}
