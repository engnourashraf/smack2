//
//  LoginVC.swift
//  smack
//
//  Created by Nour on 9/25/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.isHidden = true
        userNameTF.setPlaceHolderColor(text: "Email", color: placeHolderColor)
        passwordTF.setPlaceHolderColor(text: "Password", color: placeHolderColor)
        // Do any additional setup after loading the view.
    }


    @IBAction func closeBtnClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createAccountClick(_ sender: Any) {
        performSegue(withIdentifier: CreateAccountSegue, sender: nil)
    }
    @IBAction func loginBtnClick(_ sender: Any) {
        loginNow()
    }
    
    
    func loginNow() {
        guard let email = self.userNameTF.text else {
            return
        }
    
        guard let password = self.passwordTF.text else {
            return
        }
        loadingView.isHidden = false
        loadingView.startAnimating()
        ServiceHelper.instance.loginUser(email: email, password: password) { (success) in
            if success {
            print("Login Successfully")
            ServiceHelper.instance.FindUserByEmail(compliation: { (success) in
                if success {
                    self.loadingView.isHidden = true
                    self.loadingView.stopAnimating()
                    print("FindUserByEmail Successfully")
                    NotificationCenter.default.post(name: LoginStatusNotify, object: nil)
                    self.dismiss(animated: true, completion: nil)
                }
            })
            }
        }
    }
}
