//
//  AddChannelVC.swift
//  smack
//
//  Created by Nour on 10/1/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class AddChannelVC: UIViewController {

    @IBOutlet weak var descriptionTF: UITextField!
    
    @IBOutlet weak var channelNameTF: UITextField!
    @IBOutlet weak var bgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionTF.setPlaceHolderColor(text: "description", color: placeHolderColor)
        channelNameTF.setPlaceHolderColor(text: "channel name", color: placeHolderColor)
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(finishScreen))
        bgView.addGestureRecognizer(tap)
    }
    @IBAction func closeBtnClick(_ sender: Any) {
            finishScreen()
    }
    
    @IBAction func createBtnClick(_ sender: Any) {
        guard let channelName = channelNameTF.text else {
            return
        }
        
        guard let channelDes = descriptionTF.text else {
            return
        }
        ServiceSocketHelper.instance.addChannel(channelName: channelName, description: channelDes) { (success) in
            if success {
                self.finishScreen()
            }
        }
    }
    
    @objc func finishScreen(){
        dismiss(animated: true, completion: nil)
    }
}
