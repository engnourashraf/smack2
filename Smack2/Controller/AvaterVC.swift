//
//  AvaterVC.swift
//  smack
//
//  Created by Nour on 9/30/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class AvaterVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var type :AvaterColorType = .dark
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 28
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: avaterCellReuseId, for: indexPath) as? AvaterCell{
            cell.configureCell(index: indexPath.item, type: type)
            return cell
        }
        return AvaterCell()
    }
    

    @IBOutlet weak var imgaesCollection: UICollectionView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var colorSegmented: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        imgaesCollection.delegate = self
        imgaesCollection.dataSource = self
    }

    @IBAction func colorSegmentClick(_ sender: Any) {
        if colorSegmented.selectedSegmentIndex == 0 {
            type = .dark
        }else{
            type = .light
        }
        imgaesCollection.reloadData()
    }
    
    @IBAction func backClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if type == .dark {
            UserDataModel.instance.setAvaterName(avaterName: "dark\(indexPath.item)")
        }else{
            UserDataModel.instance.setAvaterName(avaterName: "light\(indexPath.item)")
        }
        dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var numOfColumes : CGFloat = 3
        if UIScreen.main.bounds.width > 320 {
            numOfColumes = 4
        }
        let spaceBetweenCells : CGFloat = 10
        let Padding : CGFloat = 40
        let cellDim = ((imgaesCollection.bounds.width-Padding) - (numOfColumes-1) * spaceBetweenCells) / numOfColumes
        return CGSize.init(width: cellDim, height: cellDim)
    }
    
    
    
    
    
    
    
    
}
