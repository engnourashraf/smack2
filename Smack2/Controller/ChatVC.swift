//
//  ChatVC.swift
//  smack
//
//  Created by Nour on 9/25/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class ChatVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(ServiceHelper.instance.Messages.count)
        return ServiceHelper.instance.Messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell =  tableView.dequeueReusableCell(withIdentifier: messageCellReuseId) as? MessageCell {
            let message = ServiceHelper.instance.Messages[indexPath.item]
            cell.configureCell(message: message)
            return cell
        }else{
            return MessageCell()
        }
    }
    
    @IBOutlet weak var typingLB: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var messagesTF: UITableView!
    @IBOutlet weak var sendMessageTF: UITextField!
    
    @IBAction func sendMessageTFChanged(_ sender: Any) {
        if sendMessageTF.text == ""{
            sendBtn.isHidden = true
            ServiceSocketHelper.instance.stopTyping()
        }else{
            sendBtn.isHidden = false
            ServiceSocketHelper.instance.startTyping()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        sendBtn.isHidden = true
        typingLB.text = ""
        view.bindToKeyboard()
        messagesTF.delegate = self
        messagesTF.dataSource = self
        messagesTF.estimatedRowHeight = 80
        messagesTF.rowHeight = UITableView.automaticDimension
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeKeyboard))
        view.addGestureRecognizer(tap)
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(selectedChannel), name: ChannelSelectedNotify, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginStatusChanged), name: LoginStatusNotify, object: nil)
        ServiceSocketHelper.instance.getMessage { (success) in
            if success {
                self.messagesTF.reloadData()
                if ServiceHelper.instance.Messages.count > 0 {
                    let endIndex = IndexPath.init(row: ServiceHelper.instance.Messages.count - 1, section: 0)
                    self.messagesTF.scrollToRow(at: endIndex, at: .bottom, animated: false)
                }
            }
        }
        ServiceSocketHelper.instance.getTypingUsers { (typingUsers) in
            guard let channelId = ServiceHelper.instance.selectedChannel._id else {return}
            var names = ""
            var numOfTypers = 0
            for (typingUser,channel) in typingUsers {
                if typingUser != UserDataModel.instance.name && channel == channelId {
                    if names == ""{
                        names = typingUser
                    }else {
                        names = "\(names), \(typingUser)"
                    }
                    numOfTypers += 1
                }
            }
            if numOfTypers > 0 && UserDefaultsHelper.instance.LoggedIn {
                var verb = "is"
                if numOfTypers > 1 {
                    verb = "are"
                }
                self.typingLB.text = "\(names) \(verb) typing a message"
            }else{
                self.typingLB.text = ""
            }
            
        }
    }
    
    @objc func LoginStatusChanged() {
        if UserDefaultsHelper.instance.LoggedIn {
        
        }else{
            self.messagesTF.reloadData()
        }
    }
    
    @objc func selectedChannel(){
        updateChannel()
    }

    @IBAction func sendMessageBtnClick(_ sender: Any) {
        if UserDefaultsHelper.instance.LoggedIn {
            guard let channelId = ServiceHelper.instance.selectedChannel._id else {return}
            guard let message = self.sendMessageTF.text else {return}
            ServiceSocketHelper.instance.addMessage(messageBody: message, channelId: channelId, userId: UserDataModel.instance.id!, compliation: { (success) in
                if success {
                    self.sendMessageTF.text = ""
                    self.sendBtn.isHidden = true
                    self.sendMessageTF.resignFirstResponder()
                    ServiceSocketHelper.instance.stopTyping()
                }
            })
        }
    }
    func updateChannel() {
        let channelName = ServiceHelper.instance.selectedChannel.name!
        titleLB.text = "#\(channelName)"
        getMessages()
    }
    
    func getMessages()  {
        ServiceHelper.instance.FindMessageByChannel(channelId: ServiceHelper.instance.selectedChannel._id) { (success) in
            if success {
                self.messagesTF.reloadData()
            }
        }
    }
    
    @objc func closeKeyboard()  {
        view.endEditing(true)
    }
}
