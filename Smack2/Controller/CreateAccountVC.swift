//
//  CreateAccountVC.swift
//  smack
//
//  Created by Nour on 9/25/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var avaterImage: UIImageView!
    var avaterName = "profileDefault"
    var avaterColor = "[0.5,0.5,0.5,1]"
    var bgColor : UIColor?
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.setPlaceHolderColor(text: "Email", color: placeHolderColor)
        passwordField.setPlaceHolderColor(text: "Password", color: placeHolderColor)
        userNameField.setPlaceHolderColor(text: "UserName", color: placeHolderColor)
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tap)
        loadingIndicator.isHidden = true
    }
    
    @objc func handleTap()  {
        view.endEditing(true)
    }

    override func viewDidAppear(_ animated: Bool) {
        let avImage = UserDataModel.instance.avaterName
        if  avImage != nil {
            avaterImage.image = UIImage.init(named: avImage!)
            avaterName = avImage!
            if (avImage?.contains("light"))! && bgColor == nil {
                avaterImage.backgroundColor = UIColor.lightGray
            }
        }
    }
    @IBAction func closeBtnClick(_ sender: Any) {
        performSegue(withIdentifier: BackToChannelsSegue, sender: nil)
    }
    @IBAction func chooseAvaterBtnClick(_ sender: Any) {
        performSegue(withIdentifier: AvaterSegue, sender: nil)
    }
    @IBAction func bgColorBtnClick(_ sender: Any) {
        let r = CGFloat(arc4random_uniform(255))/255
        let g = CGFloat(arc4random_uniform(255))/255
        let b = CGFloat(arc4random_uniform(255))/255
        bgColor = UIColor.init(red: r, green: g, blue: b, alpha: 1)
        UIView.animate(withDuration: 0.2){
        self.avaterImage.backgroundColor = self.bgColor
        }
        self.avaterColor = "[\(r),\(g),\(b),1]"
    }
    @IBAction func createAccountBtnClick(_ sender: Any) {
        self.loadingIndicator.isHidden = false
        self.loadingIndicator.startAnimating()
        guard let email = emailField.text,emailField.text != "" else { return }
        guard let password = passwordField.text,passwordField.text != "" else { return }
        guard let name = userNameField.text,userNameField.text != "" else { return }
        ServiceHelper.instance.registerUser(email: email, password: password) { (success) in
            if success {
                print("Register Successfully")
                ServiceHelper.instance.loginUser(email: email, password: password, compliation: { (success) in
                    if success {
                        print("Login Successfully")
                        print(UserDefaultsHelper.instance.UserEmail)
                        ServiceHelper.instance.createUser(email: email, userName: name, avatarName: self.avaterName, avatarColor: self.avaterColor, compliation: { (success) in
                            if success {
                                self.loadingIndicator.isHidden = true
                                self.loadingIndicator.stopAnimating()
                                print("Created Successfully")
                                self.performSegue(withIdentifier: BackToChannelsSegue, sender: nil)
                                NotificationCenter.default.post(name: LoginStatusNotify, object: nil)
                            }
                        })
                    }
                })
            }
        }
    }
    
}
