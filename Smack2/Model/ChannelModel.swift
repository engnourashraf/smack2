//
//  ChannelModel.swift
//  smack
//
//  Created by Nour on 10/1/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import Foundation

struct ChannelModel : Decodable {
    public private(set) var name : String!
    public private(set) var description : String!
    public private(set) var _id : String!
    public private(set) var __v : Int?
}
