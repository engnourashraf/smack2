//
//  UserDataModel.swift
//  smack
//
//  Created by Nour on 9/26/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import Foundation
class UserDataModel {
    
    static let instance = UserDataModel()
    public private(set) var name : String?
    public private(set) var id :String?
    public private(set) var avaterColor :String?
    public private(set) var avaterName :String?
    public private(set) var email :String?
    
    func setUserData(name:String,id:String,avaterColor:String,avaterName:String,email:String) {
        self.id = id
        self.avaterColor = avaterColor
        self.name = name
        self.email = email
        self.avaterName = avaterName
    }
    
    func setAvaterName(avaterName :String)  {
        self.avaterName = avaterName
    }
    
    func logout()  {
        self.id = nil
        self.avaterColor = nil
        self.avaterName = nil
        self.name = nil
        self.email = nil
        UserDefaultsHelper.instance.LoggedIn = false
        UserDefaultsHelper.instance.Token = ""
        UserDefaultsHelper.instance.UserEmail = ""
        ServiceHelper.instance.Channels.removeAll()
        ServiceHelper.instance.Messages.removeAll()
    }
    
}
