//
//  MessageModel.swift
//  smack
//
//  Created by Nour on 10/2/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import Foundation
struct MessageModel : Decodable {
    public private(set) var userName : String!
    public private(set) var messageBody : String!
    public private(set) var userId : String!
    public private(set) var channelId : String!
    public private(set) var userAvatar : String!
    public private(set) var userAvatarColor : String!
    public private(set) var _id : String!
    public private(set) var timeStamp : String!
    public private(set) var __v : Int?
}
