//
//  AvaterColorType.swift
//  smack
//
//  Created by Nour on 10/1/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import Foundation
enum AvaterColorType {
    case dark
    case light
}
